# Sample ChessCorp Application

## Abstract

This project demonstrates how to integrate some modules to create a simple Chess user interface.

It uses the following modules from [ChessCorp project](http://gitlab.com/ChessCorp):

* Chess Board web component [View](http://gitlab.com/ChessCorp/chess-board)
* Chess random artificial intelligence [View](http://gitlab.com/ChessCorp/chess-ai-random)

The interface displays an interactive board where you play white and an artificial intelligence plays black.

## Test it

You can test it on [http://chesscorp.gitlab.io/chess-sample-ui/](http://chesscorp.gitlab.io/chess-sample-ui/)
