[![NPM version][npm-image]][npm-url] [![Build Status][travis-image]][travis-url] [![Dependency Status][daviddm-image]][daviddm-url] [![Coverage Status](https://coveralls.io/repos/ChessCorp/chess-ai-random/badge.svg?branch=master&service=github)](https://coveralls.io/github/ChessCorp/chess-ai-random?branch=master)



# chess-ai-random
A basic Chess AI playing completely randomly. Mostly useful for scaffolding a new AI project.

[npm-image]: https://badge.fury.io/js/chess-ai-random.svg
[npm-url]: https://npmjs.org/package/chess-ai-random
[travis-image]: https://travis-ci.org/ChessCorp/chess-ai-random.svg?branch=master
[travis-url]: https://travis-ci.org/ChessCorp/chess-ai-random
[daviddm-image]: https://david-dm.org/ChessCorp/chess-ai-random.svg?theme=shields.io
[daviddm-url]: https://david-dm.org/ChessCorp/chess-ai-random
